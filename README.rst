.. image:: https://travis-ci.org/mcepl/ofxstatement-czech.svg?branch=master
    :target: https://travis-ci.org/mcepl/ofxstatement-czech

This is a collection of parsers for propriatory statement formats, produced by
certain Czech banks. It is a plugin for `ofxstatement`_.

.. _ofxstatement: https://github.com/kedder/ofxstatement
