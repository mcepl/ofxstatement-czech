let g:flake8_cmd="/usr/bin/python3-flake8"
let g:syntastic_python_python_exe = 'python3'
let g:syntastic_python_pylint_exe = 'python3-pylint'
